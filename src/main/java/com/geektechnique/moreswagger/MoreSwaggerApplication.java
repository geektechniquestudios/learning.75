package com.geektechnique.moreswagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoreSwaggerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MoreSwaggerApplication.class, args);
    }

}
